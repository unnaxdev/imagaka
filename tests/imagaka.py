import unittest
from imagaka import Model

class TestImagaka(unittest.TestCase):
    def test_model(self):
        training_data = {
            "incoming": [{
                "concept": "PAG NOMINAS - 2942-56-0001580-44 000045 000001",
                "category": "Salary"
            }, {
                "concept": "Description: Devolucion Tarjeta WWW.OKMONEY.ES INTERNET ES",
                "category": "OK Money"
            }],
            "outgoing": [{
                "concept": "COMPRA EN JOSEFA MELENDO CABELLO, CON LA TARJET...",
                "category": "Other outgoing payments"
            }, {
                "concept": "Description: COMPRA EN PAYPAL *WAVES INC WAVE, 35314369001, TARJETA **************** , COMISION 0,00",
                "category": "Online payments"
            }]
        }
        model = Model(training_data['incoming'], training_data['outgoing'], vectorizer_algorithm='hashing')

        test_statements = [{
            "amount": -234,
            "concepts": ["COMPRA EN JOSEFA MELENDO CABELLO, CON LA TARJET..."]
        }, {
            "amount": 2340,
            "concepts": ["PAG NOMINAS - 2942-56-0001580-44 000045 000001"]
        }]
        statements_cat = model.categorize(test_statements)
        self.assertTrue(statements_cat[0]['category']['title'])
        self.assertTrue(statements_cat[1]['category']['title'])

    def test_no_change_statements(self):
        training_data = {
            "incoming": [{
                "concept": "PAG NOMINAS - 2942-56-0001580-44 000045 000001",
                "category": "Salary"
            }, {
                "concept": "Description: Devolucion Tarjeta WWW.OKMONEY.ES INTERNET ES",
                "category": "OK Money"
            }],
            "outgoing": [{
                "concept": "COMPRA EN JOSEFA MELENDO CABELLO, CON LA TARJET...",
                "category": "Other outgoing payments"
            }, {
                "concept": "Description: COMPRA EN PAYPAL *WAVES INC WAVE, 35314369001, TARJETA **************** , COMISION 0,00",
                "category": "Online payments"
            }]
        }
        model = Model(training_data['incoming'], training_data['outgoing'], vectorizer_algorithm='hashing')
        test_statements = [{
            "amount": -234,
            "concepts": ["COMPRA EN JOSEFA MELENDO CABELLO, CON LA TARJET..."]
        }, {
            "amount": 2340,
            "concepts": ["PAG NOMINAS - 2942-56-0001580-44 000045 000001"]
        }]
        statements_cat = model.categorize(test_statements)
        self.assertTrue("category" not in test_statements[0].keys())
        self.assertNotEqual(statements_cat, test_statements)


if __name__ == '__main__':
    unittest.main()
