import logging
from imagaka import create_model

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

statements = [{
    "amount": -234,
    "concepts": ["COMPRA EN JOSEFA MELENDO CABELLO, CON LA TARJET..."]
}, {
    "amount": 2340,
    "concepts": ["PAG NOMINAS - 2942-56-0001580-44 000045 000001"]
}]

model = create_model()

if __name__ == "__main__":
    # Example
    logger.info("Categorize statements")
    statements_cat = model.categorize(statements)

    for statement in statements_cat:
        child_category = statement['category']['title']
        concept = ' | '.join(statement['concepts'])
        print('{}: {}'.format(child_category, concept))
