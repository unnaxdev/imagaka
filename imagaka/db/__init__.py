import os
import json
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class DB:
    def __init__(self):
        pass

    def get_training_data(self):
        return None


class FilesDB(DB):
    def __init__(self, dir_training_data=None):
        logger.info("Init FilesDB")
        if not dir_training_data:
            CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
            dir_training_data = os.path.join(CURRENT_DIR, '..', 'training_data')

        self.filename_incoming_data = os.path.join(dir_training_data, 'positive_amount_min.json')
        self.filename_outgoing_data = os.path.join(dir_training_data, 'negative_amount_min.json')

    def get_training_data(self):
        logger.info("Loading incoming data")
        with open(self.filename_incoming_data) as f:
            incoming_data = json.load(f)

        logger.info("Loading outgoing data")
        with open(self.filename_outgoing_data) as f:
            outgoing_data = json.load(f)

        return incoming_data, outgoing_data


def get_db(*args, **kwargs):
    return FilesDB(*args, **kwargs)
