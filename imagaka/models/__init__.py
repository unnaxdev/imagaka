class Category:
    def __init__(self, map_category_tree, child_category):
        # FIXME handle key error, void map_category_tree
        self.data = map_category_tree[child_category]

    @property
    def major_category(self):
        return self.data['major_category']

    @property
    def parent_category(self):
        return self.data['parent_category']

    @property
    def child_category(self):
        return self.data['child_category']

    def to_dict(self): # Get category
        return {
            "title": self.child_category,
            "hierarchy": [{
                "title": self.major_category
            },{
                "title": self.parent_category
            },{
                "title": self.child_category
            }]
        }


class Statement:
    def __init__(self, statement_dict):
        self.data = statement_dict

    @property
    def concept(self):
        return ' | '.join(self.data['concepts'])

    @property
    def category(self):
        return self.data['category']['hierarchy'][-1]['title']

    def get(self, name):
        return self.data.get(name)


def create_category_map(category_tree):
    # mapping for fast search
    map_category_tree = {}
    for major_category in category_tree:
        for parent_category in category_tree[major_category]:
            for child_category in category_tree[major_category][parent_category]:
                # Simple check (not child_category repeated)
                if child_category in map_category_tree:
                    raise ValueError('Repeated category {}'.format(child_category))
                map_category_tree[child_category] = {'major_category': major_category, 'parent_category': parent_category, 'child_category': child_category}
    return map_category_tree
