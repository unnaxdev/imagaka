import os
import logging
import pickle
import hashlib
import json
import numpy as np
from copy import deepcopy
from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import CountVectorizer

from .models import create_category_map, Category

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Categorizer:
    def __init__(self, clf, clf_args, vec, vec_args):
        self.clf = clf(**clf_args)
        self.vectorizer = vec(**vec_args)

    def train(self, concepts, categories):
        logger.info("Training init")

        X_raw = np.array(concepts)
        y_train = np.array(categories)

        # Train vectorizer
        logger.info("Training vectorizer")
        X_train = self.vectorizer.fit_transform(X_raw)

        # Train classifier
        logger.info("Training classifier")
        self.clf.fit(X_train, y_train)

    def predict(self, concepts):
        logger.info("Predict %d concepts", len(concepts))
        if not len(concepts):
            return None

        X_raw_test = np.array(concepts)
        X_test = self.vectorizer.transform(X_raw_test)
        return self.clf.predict(X_test)

    def categorize(self, statements, category_map_fn=None):
        def join_concepts(concepts):
            return ' | '.join(concepts)
        def extract_concepts(statements):
            return [join_concepts(statement['concepts']) for statement in statements]

        logger.info("Categorize init: %d statements", len(statements))
        if not len(statements):
            return []

        concepts = extract_concepts(statements)
        categories = self.predict(concepts)

        for idx, statement in enumerate(statements):
            category = categories[idx]
            if category_map_fn:
                category = category_map_fn(category)
            statement['category'] = category
        logger.info("Categorize done:")
        return statements


class Model:
    def __init__(self, _incoming_args, _outgoing_args, cache_dir=None, validate_cache=False):
        base_args = {
            "clf": LinearSVC,
            "vec": HashingVectorizer,  #CountVectorizer or HashingVectorizer
            "clf_args": {"penalty":"l1", "dual":False, "tol":1e-3},
            "vec_args": {'ngram_range': (1, 3), 'strip_accents': 'ascii', 'token_pattern': u'(?u)\\b\\w+\\b'},
            "data": None,  #Required if not cache or validate_cache = True
            "category_tree": None  #Required
        }
        incoming_args = base_args.copy()
        outgoing_args = base_args.copy()

        incoming_args.update(_incoming_args or {})
        outgoing_args.update(_outgoing_args or {})

        logger.info("Init model")
        self.cache_dir = cache_dir
        self.validate_cache = validate_cache

        self.map_incoming_category_tree = create_category_map(incoming_args['category_tree'])
        self.map_outgoing_category_tree = create_category_map(outgoing_args['category_tree'])

        self.incoming_model = self.init_model(
            'incoming', incoming_args['data'],
            Categorizer(
                incoming_args['clf'],
                incoming_args['clf_args'],
                incoming_args['vec'],
                incoming_args['vec_args']
            )
        )
        self.outgoing_model = self.init_model(
            'outgoing', outgoing_args['data'],
            Categorizer(
                outgoing_args['clf'],
                outgoing_args['clf_args'],
                outgoing_args['vec'],
                outgoing_args['vec_args']
            )
        )

    def init_model(self, name, data, model):
        # Init model
        # Training a model is very time consuming so if the data has not changed
        # is better to load the model from cache

        # Try to load model from cache
        trained_model = self.load_model(name, data)
        if trained_model:
            return trained_model

        logger.info("Training %s_model", name)
        try:
            concepts = []
            categories = []
            for train_data in data:
                concepts.append(train_data['concept'])
                categories.append(train_data['category'])
        except:
            raise Exception("Wrong training data structure")

        model.train(concepts, categories)

        # Save model in cache
        self.save_model(model, data, name)

        return model

    def save_model(self, model, data, name):
        if not self.cache_dir:
            return

        filename = os.path.join(self.cache_dir, name + '_model.pkl')
        # Save model in .cache
        # Create .cache dir if necessary
        if not os.path.isdir(self.cache_dir):
            os.makedirs(self.cache_dir)

        logger.info("Saving model in %s", filename)
        with open(filename, 'wb') as cache_file:
            #TODO try joblib to save efficiently on Python objects containing large data (http://scikit-learn.org/stable/modules/model_persistence.html)
            pickle.dump({
                'model': model,
                'hash': self.get_hash(data)
            }, cache_file)

    def get_hash(self, data):
        return hashlib.md5(
            json.dumps(data, sort_keys=True).encode('utf-8')
        ).hexdigest()

    def validate_cache_model(self, cache_model, data):
        return cache_model['hash'] == self.get_hash(data)

    def load_model(self, name, data):
        if not self.cache_dir:
            return None

        filename = os.path.join(self.cache_dir, name + '_model.pkl')
        # Load model from .cache
        try:
            with open(filename, 'rb') as cache_file:
                cache_model = pickle.load(cache_file)

            if self.validate_cache and not self.validate_cache_model(cache_model, data):
                return None

            logger.info("Loading model from %s", filename)
            return cache_model['model']
        except:
            return None

    def categorize(self, statements):
        logger.info("Categorize %d statements", len(statements))
        if not len(statements):
            return []

        statements = deepcopy(statements)
        incoming_statements = []
        outgoing_statements = []
        for statement in statements:
            if statement['amount'] > 0:
                incoming_statements.append(statement)
                continue
            outgoing_statements.append(statement)

        if len(incoming_statements):
            self.incoming_model.categorize(incoming_statements, category_map_fn=lambda child_category: Category(self.map_incoming_category_tree, child_category).to_dict())  # set category inplace

        if len(outgoing_statements):
            self.outgoing_model.categorize(outgoing_statements, category_map_fn=lambda child_category: Category(self.map_outgoing_category_tree, child_category).to_dict())  # set category inplace
        logger.info("Categorize done")
        return statements


#def create_model():
#    from .db import get_db
#    database = get_db()
#    incoming_data, outgoing_data = database.get_training_data()
#
#    CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
#    cache_dir = os.path.join(CURRENT_DIR, '.cache')
#    return Model(
#        {
#            "data": incoming_data,  #Required
#            "cat_model": create_incoming_category  #Required
#        }, {
#            "data": outgoing_data,  #Required
#            "cat_model": create_outgoing_category  #Required
#        }, cache_dir, validate_cache=False)
