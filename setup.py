from setuptools import setup

setup(name='imagaka',
      version='0.0.5',
      description='Classifier of statements',
      url='https://bitbucket.org/unnaxdev/imagaka',
      author='Unnax',
      author_email='developers@unnax.com',
      license='MIT',
      packages=['imagaka', 'imagaka.db', 'imagaka.models'],
      zip_safe=False,
      install_requires=[
          'numpy==1.12.0',
          'scikit-learn==0.18.1',
          'scipy==0.18.1',
          'sklearn==0.0'
      ])
